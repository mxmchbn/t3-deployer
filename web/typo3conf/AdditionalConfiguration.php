<?php

/**
 * BE configuration
 */
if (getenv('TYPO3_BE__INSTALL_TOOL_PASSWORD') !== false) {
    $GLOBALS['TYPO3_CONF_VARS']['BE']['installToolPassword'] = getenv('TYPO3__BE__INSTALL_TOOL_PASSWORD');
}

/**
 * DB configuration
 */
if (getenv('TYPO3__DB__HOST') !== false) {
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = getenv('TYPO3__DB__HOST');
}
if (getenv('TYPO3__DB__NAME') !== false) {
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = getenv('TYPO3__DB__NAME');
}
if (getenv('TYPO3__DB__USER') !== false) {
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = getenv('TYPO3__DB__USER');
}
if (getenv('TYPO3__DB__PASSWORD') !== false) {
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = getenv('TYPO3__DB__PASSWORD');
}
if (getenv('TYPO3__DB__UNIX_SOCKET') !== false) {
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['unix_socket'] = getenv('TYPO3__DB__UNIX_SOCKET');
}
if (getenv('TYPO3__DB__PORT') !== false) {
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['port'] = getenv('TYPO3__DB__PORT');
}

/**
 * SYS configuration
 */
if (getenv('TYPO3__SYS__ENCRYPTION_KEY') !== false) {
    $GLOBALS['TYPO3_CONF_VARS']['BE']['encryptionKey'] = getenv('TYPO3__SYS__ENCRYPTION_KEY');
}
